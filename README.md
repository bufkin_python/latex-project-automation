# LaTeX project automation

This script allows you to create a new LaTeX file from several prebuilt templates.

All you have to do is create a file called ".env" in the source folder with a variable called DATA_FOLDER and the path to the data folder of this project.

---

Available templates for new projects are:

- Basic
- Math
- Vorstandssitzung
- TikZ
- Monatsbericht
