#! /usr/bin/env/python311

import stat
import sys
from os import chmod, getcwd, getenv, path, walk

from PyInquirer import prompt
from dotenv import load_dotenv


class LatexCreation:
    """ Main class of this project."""
    topics = ["Basic", "Math", "Vorstandssitzung",
              "TikZ", "Monatsbericht", "Wochenbericht"]
    data_folder = ""

    def __init__(self, data_folder):
        """ Initialization method for this class. """
        self.data_folder = data_folder

    def load_env(self):
        """ Loading user settings from .env file. """
        load_dotenv("../.env")
        self.data_folder = str(getenv("DATA_FOLDER"))
        print(self.data_folder)

    def data_folder_end_check(self):
        """ Ensure that the path of the data folder ends with a slash. """
        if not self.data_folder.endswith("/"):
            self.data_folder += "/"

    def data_folder_not_null(self):
        """ This method checks if the path to the data folder in the .env file is not empty. """
        if not self.data_folder:
            print("Data folder could not be loaded...")
            sys.exit()

    def create_new_latex(self):
        """ Asks for the topic and creates the correct latex file. """
        questions = [
            {
                "type": "list",
                "name": "Topic",
                "message": "What theme should the new file have?",
                "choices": self.topics
            }
        ]

        user_input = prompt(questions)

        topic = user_input.get("Topic")
        load_dotenv()
        self.data_folder_end_check()
        self.data_folder_not_null()

        for root, dirs, files in walk(self.data_folder, topdown=False):
            for _dir in [self.data_folder.join(root) for _ in dirs]:
                chmod(_dir, stat.S_IWOTH)

            for input_file in [self.data_folder.join(root) for _ in files]:
                chmod(input_file, stat.S_IWOTH)

        title = input("Title? ")
        input_file = f"{str(topic).lower()}_latex.tex"

        path.abspath(getcwd())
        data_file = str(self.data_folder) + input_file

        with open(data_file, 'wb') as input_file:
            tex_content = input_file.read()

            new_title = f"{title}.tex"

            with open(new_title, 'wb') as _input_file:
                _input_file.write(tex_content)


if __name__ == "__main__":
    latex = LatexCreation("../data/")
    latex.create_new_latex()
